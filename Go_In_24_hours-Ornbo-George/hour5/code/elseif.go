package main

import (
	"fmt"
)

func verifyNumber(number *int) {
	if *number == 0 {
		fmt.Println("Really Number ZERO")
	} else if *number == 1 {
		fmt.Println("Really Number ONE")
	} else if *number == 2 {
		fmt.Println("Really Number TWO")
	} else {
		fmt.Println("NO NUMBER")
	}
}

func main() {
	a, b, c, d := 0, 1, 2, 3
	verifyNumber(&a)
	verifyNumber(&b)
	verifyNumber(&c)
	verifyNumber(&d)
}
