package main

import (
	"fmt"
)

func main() {
	for i := 1; i < 10; i++ {
		for J := 1; J < 10; J++ {
			fmt.Printf("\t%v x %v = %v \n", i, J, (i * J))
		}
		fmt.Printf("\n")
	}

	var beatles [4]string = [4]string{"John", "Paul", "Ringo", "George"}
	for i, p := range beatles {
		fmt.Printf("\tBeatle in array %v is %v\n", i, p)
	}
}
