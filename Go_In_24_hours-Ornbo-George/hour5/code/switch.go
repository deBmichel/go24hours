package main

import (
	"fmt"
)

func verifyNumber(number *int) {
	switch *number {
	case 0:
		fmt.Println("Really Number ZERO")
	case 1:
		fmt.Println("Really Number ONE")
	case 2:
		fmt.Println("Really Number TWO")
	default:
		fmt.Println("NO NUMBER")
	}
}

func main() {
	a, b, c, d := 0, 1, 2, 3
	verifyNumber(&a)
	verifyNumber(&b)
	verifyNumber(&c)
	verifyNumber(&d)
}
