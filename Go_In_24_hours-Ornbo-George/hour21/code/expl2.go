package main

import (
	"io/ioutil"
	"log"
)

func main() {
	b := make([]byte, 0)
	// Using a trick to create a file with empthy []byte
	err := ioutil.WriteFile("exampleAll.txt", b, 0777)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Created file")
}
