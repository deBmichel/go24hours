package main

import (
	"fmt"
	"io"
	"log"
	"os"
)

func main() {
	from, err := os.Open("./expl5.go")
	if err != nil {
		log.Fatal(err)
	}
	defer from.Close()

	to, err := os.OpenFile("./example05.copy.txt", os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		log.Fatal(err)
	}
	defer to.Close()

	_, err = io.Copy(to, from)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Finished Operation")
}
