package main

import (
	"fmt"
)

func main() {
	var cheeses [2]string
	cheeses[0] = "Mariolles"
	cheeses[1] = "Époisses de Bourgogne"

	fmt.Println(cheeses[0])
	fmt.Println(cheeses[1])
	fmt.Println(cheeses)

	var pieces [4]string = [...]string{"pawn", "tower", "knight", "bishop"}
	fmt.Println(pieces)
}
