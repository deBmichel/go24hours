package main

import (
	"fmt"
)

func main() {
	var pieces = make([]string, 2)
	pieces[0] = "king"
	pieces[1] = "Queen"
	pieces = append(pieces, "Tower", "Pawn", "Bishop", "knight")
	fmt.Println(pieces)
	fmt.Println("Deleting", pieces[3])
	pieces = append(pieces[:3], pieces[3+1])
	fmt.Println(pieces)
	var copyPieces = make([]string, 2)
	copy(copyPieces, pieces)
	fmt.Println("pieces", pieces)
	fmt.Println("copyPieces", copyPieces)
}
