package main

import (
	"fmt"
	"reflect"
)

type T struct {
	a int64
	b int64
}

func main() {
	var i int
	var f float64
	var b bool
	var s string
	var a T
	fmt.Printf("%v %v %v %q\n", i, f, b, s)
	fmt.Println(a)

	x, y, z := "a", 1.0, 3
	xx, yy, zz := reflect.TypeOf(x), reflect.TypeOf(y), reflect.TypeOf(z)
	fmt.Println(xx, yy, zz)
}
