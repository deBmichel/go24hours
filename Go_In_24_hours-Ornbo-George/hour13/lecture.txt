Using packages for code reuse:

A Go package is used to group code.

Go package names are short, concise, and evocative.

You can use third-party packages but you should be carefull.

You should review the package and people using the package
remember: A running package has access to the operating 
system.

The go get command supports updating individual or all 
packages on a filesystem. To update dependencies for a 
project, the following may be run from within a project folder:

	go get -u

Specific packages may also be updated:
	
	go get -u github.com/spf13/hugo

All packages on a filesystem may also be updated:

	go get -u all

You can use the vendor folder to play with dependencies.

I preffer go modules sure :

https://go.dev/doc/tutorial/call-module-code
https://go.dev/doc/tutorial/create-module
