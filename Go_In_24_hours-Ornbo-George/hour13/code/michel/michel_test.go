package michel_test

import (
	"example.com/michel"
	"testing"
)

func TestName(t *testing.T) {
	if michel.Name() != "Michel" {
		t.Errorf("expected %v, actual %v", "Michel", michel.Name())
	}
}
