package main

import (
	"bytes"
	"fmt"
	"strconv"
	"strings"
)

func main() {
	s := "Interpreted ... \t hell \n you."
	fmt.Println(s)
	ss := `Hello 
	how are you 
	text hello \t world`
	fmt.Println(ss)
	f, o := s+ss, 9
	fmt.Println(f)
	xx := strconv.Itoa(o) + "Michel"
	fmt.Println(xx)

	var buffer bytes.Buffer
	for i := 0; i < 500; i++ {
		buffer.WriteString("z")
	}
	fmt.Println(buffer.String())
	fmt.Println(strings.ToLower("VERY IMPORTANT MESSAGE"))
	fmt.Println(strings.Index("surface", "face"))
	fmt.Println(strings.Index("moon", "aer"))
	fmt.Println(strings.TrimSpace("   I don't need all this space     "))

}
