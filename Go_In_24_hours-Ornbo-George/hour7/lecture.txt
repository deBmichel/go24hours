Using Structs and Pointers:

A struct is a structure of data elements that is a useful 
programming construct.

A struct is a collection of data fields with declared data 
types.

It is not possible, however, to change the types in a struct 
after it has been declared or an instance has been created.

Using short variable assignment is the most common—and 
recommended—way of creating a struct.

Structs are not a way to create classes or object-oriented 
code. Structs are a way to create data structures that are 
relevant to the data you are trying to model.

Nesting one struct within another can be a useful way to 
model more complex structures.

tructs of the same type can be compared using Go’s equality 
operators. To assess whether structs are the same, == can be 
used to test for equality and != to test for inequality.

It is not possible to compare structs of different types, 
and attempting to do so will result in a compile time error.

Both structs and elements within structs can be exported or 
not exported according to the Go convention. This is so that 
identifiers that start with an uppercase letter are exported, 
and those without are not.
For the struct and elements within it to be exported, the 
struct name and the elements within it must begin with an 
uppercase letter.

You can copy using := and it is easy.

A pointer is a reference to the memory address, so rather 
than operating on a copy of the struct, modifications will 
update any variable referencing the underlying memory. To 
reference a pointer, the variable name is preceded with an 
ampersand.

The difference between pointers and values is a subtle one, 
but choosing whether to use a pointer or a value is simple. 
If you need to modify (or mutate) the original initialization 
of a struct, use a pointer. If you need to operate on a struct, 
but do not want to modify the original initialization of a 
struct, use a value.

