package main

import (
	"fmt"
)

type Drink struct {
	Name string
	Ice  bool
}

func main() {
	ad := Drink{
		Name: "Lemonade M",
		Ice:  true,
	}
	bb := ad
	bb.Ice = false
	ad.Name = "Michel"
	fmt.Printf("%+v\n", bb)
	fmt.Printf("%+v\n", ad)
	fmt.Printf("%p\n", &ad)
	fmt.Printf("%p\n", &bb)

	a := Drink{
		Name: "Lemonade",
		Ice:  true,
	}
	b := &a
	b.Ice = false
	a.Name = "MICHEL"
	fmt.Printf("%+v\n", *b)
	fmt.Printf("%+v\n", a)
	fmt.Printf("%p\n", b)
	fmt.Printf("%p\n", &a)
}
