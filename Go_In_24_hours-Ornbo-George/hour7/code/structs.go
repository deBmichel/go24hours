package main

import (
	"fmt"
	"reflect"
)

type Movie struct {
	Name   string
	Rating float64
}

type Person struct {
	Name    string
	Age     int
	Address Address
}

type Address struct {
	Latitude  float32
	Longitude float32
}

type Drink struct {
	Name string
	Ice  bool
}

func Show(M *Movie) {
	fmt.Printf("%+v\n", M)
}

func main() {
	p := Person{
		Name: "Michel",
		Age:  27,
		Address: Address{
			Latitude:  3.0,
			Longitude: 4.7,
		},
	}
	fmt.Println(p)

	m := Movie{
		Name:   "Titanic",
		Rating: 2.0,
	}
	fmt.Println(m)
	fmt.Println(m.Name, m.Rating)

	var T Movie
	fmt.Println(T)
	T.Rating = 0.9918
	fmt.Printf("%+v\n", T)

	N := new(Movie)
	N.Name = "Deadpool"
	Show(N)

	MF := Movie{"Matrix", 9.0}
	Show(&MF)

	a := Drink{
		Name: "Lemonade",
		Ice:  true,
	}
	b := Drink{
		Name: "Lemonade",
		Ice:  true,
	}
	if a == b {
		fmt.Println("a and b are the same")
	}
	fmt.Printf("%+v\n", a)
	fmt.Printf("%+v\n", b)
	fmt.Println(reflect.TypeOf(a))
	fmt.Println(reflect.TypeOf(b))
}
