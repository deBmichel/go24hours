Naming conventions in Go:

Idiomatic Go is used regularly as a reference to the 
accepted way of doing things.

There is no enforced standard or compiler checking for 
what Idiomatic Go means, and at times Idiomatic Go can 
seem mysterious and unclear.

Go takes a pragmatic but strong approach to code formatting. 
Although it is not enforced, there is a formatting convention 
for Go, and this is encapsulated in the gofmt command. The 
compiler does not enforce that code is formatted per the gofmt 
command, but almost all the Go community use gofmt and expect 
code to be formatted in this way.

It is strongly recommended that you format your code according 
to the Go conventions.

Go provides the gofmt tool to help ensure that Go code is 
formatted in line with expected conventions. The beauty of 
this tool is that you do not even need to learn the 
conventions. Over time, as you learn how code is formatted, 
adhering to the conventions will become second nature.

When writing Go code, many text editors have third-party Go 
plugins that automatically run gofmt when a file is saved and 
correct the formatting before a file is saved.

GO sublime. for sublimetext.

The convention of uppercase and lowercase should be followed.

The convention in Go is to use Camel Case or Pascal Case for 
variable names where two words need to be compounded.

var fileName // Camel Case
var FileName // Pascal Case

Without reading the implementation of a function, a programmer 
can easily understand what it does.

If you want you can use golint, but I preffer golangci-lint.

A linter is a powerfull tool, you learn about the programming
language and about the accepted conventions of the language
ecosystem.

The linter helpfully shows the lines and positions within the 
code that need attention. If you are using Vim as your text 
editor, this also integrates with the Quickfix menu. These 
recommendations are not mandatory, as the code will compile, 
but given that the Go project itself uses this tool, it is a 
good idea to try and fix the warnings and learn the conventions. 
Using the golint tool can also be a great way to learn to learn 
how to write Idiomatic Go.

Linters are a way to improve your learning in a programming 
language.

Is important to know and use godoc.

Comments are important in software development.

In GOlang you have the godoc.

to play I used command:

godoc ./example

Documentation practices for the standard libraries are a great 
way to learn how to write documentation.

I was playing with 

http://localhost:6060/pkg/ and godoc -http=":6060"

the makefile was a beautifull lesson I can run the makefile
and I learned about tabs in this type of files.

What is the idiomatic way of naming interfaces in Go?
The idiomatic way of naming interfaces is to use a verb with 
“er” on the end. This describes what the interface does. 
Examples include Parser and Authorizer.





















