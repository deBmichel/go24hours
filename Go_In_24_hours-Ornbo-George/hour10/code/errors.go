package main

import (
	"errors"
	"fmt"
	"io/ioutil"
)

func Half(n *int) (int, error) {
	if *n%2 == 0 {
		return *n / 2, nil
	} else {
		return -1, fmt.Errorf("Can not find half %v", *n)
	}
}

func main() {
	file, err := ioutil.ReadFile("foo.txt")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("%s", file)

	err = errors.New("ṔACMAN")
	if err != nil {
		fmt.Println(err)
	}

	name, role := "Richard Jupp", "Drummer"
	err = fmt.Errorf("The %v %v quit", role, name)
	if err != nil {
		fmt.Println(err)
	}

	i := 9
	n, err := Half(&i)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(n)

	panic("We are in panic , OOOOH NOO ")
	fmt.Println("No panic all is good, no problems here")
}
