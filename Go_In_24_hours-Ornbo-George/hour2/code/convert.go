package main

import (
	"fmt"
	"strconv"
)

func main() {
	s := "TRUE"
	b, err := strconv.ParseBool(s)
	fmt.Println(b, err)
	fmt.Println(strconv.FormatBool(true) + " value")
	i, err := strconv.Atoi("34")
	fmt.Println(i, err)
}
