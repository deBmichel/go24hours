package main

import (
	"errors"
	"fmt"
	"reflect"
)

func main() {
	a := func(x string) error {
		return errors.New("tipe error str arg" + x)
	}

	fmt.Println(a("mICHEL"))
	fmt.Println(reflect.TypeOf(a))
}
