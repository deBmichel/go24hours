package main

import (
	"fmt"
	"time"
)

func mic(c chan string) {
	for _, ok := (<-c); ok; {
		fmt.Println("aaa")
		time.Sleep(time.Second * 2)
	}
}

func main() {
	c := make(chan string)

	go mic(c)
	for {

	}
}
