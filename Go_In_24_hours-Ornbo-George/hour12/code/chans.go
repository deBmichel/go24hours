package main

import (
	"fmt"
	"time"
)

func slowFunc(c chan string) {
	time.Sleep(time.Second * 2)
	c <- "slowFunc() FINISHED"
}

func bufferedTest(c chan string) {
	for msg := range c {
		fmt.Println(msg)
	}
}

func main() {
	c := make(chan string)
	go slowFunc(c)
	msg := <-c
	fmt.Println(msg)

	messages := make(chan string, 3)
	messages <- "Hello"
	messages <- "World"
	messages <- "TEXT"
	close(messages)
	fmt.Println("Two messages in the channel with no receiver")
	bufferedTest(messages)
}
