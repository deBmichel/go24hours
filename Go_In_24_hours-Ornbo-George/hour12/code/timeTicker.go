package main

import (
	"fmt"
	"time"
)

func pinger(c chan string) {
	t := time.NewTicker(1 * time.Second)
	for {
		fmt.Println("out???", time.Now())
		c <- "ping"
		<-t.C
	}
	fmt.Println("was out really")
}

func main() {
	msg := make(chan string)
	go pinger(msg)
	for {
		select {
		case m := <-msg:
			fmt.Println(m)
		}
	}
}
