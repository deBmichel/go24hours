package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("Sleep")
	time.Sleep(3 * time.Second)
	fmt.Println("I'm awake")
}
