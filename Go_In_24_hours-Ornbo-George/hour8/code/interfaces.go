package main

import (
	"errors"
	"fmt"
)

type Robot interface {
	PowerOn() error
}

type T850 struct {
	Name string
}

func (R *T850) PowerOn() error {
	return nil
}

func (R *T850) FTest() {
	fmt.Println("Hello FTest")
}

type R2D2 struct {
	Broken bool
}

func (R *R2D2) PowerOn() error {
	if R.Broken {
		return errors.New("R2D2 is broken")
	}
	return nil
}

func Boot(r Robot) error {
	return r.PowerOn()
}

func main() {
	t := T850{
		Name: "The terminator.",
	}

	r := R2D2{
		Broken: true,
	}

	fmt.Println(t.Name, Boot(&t))
	fmt.Println(r.Broken, Boot(&r))

}
