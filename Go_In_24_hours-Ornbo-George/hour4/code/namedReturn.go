package main

import (
	"fmt"
)

func sayHi() (x, y string) {
	x = "hello"
	y = "Michel"
	return
}

func main() {
	fmt.Println(sayHi())
}
