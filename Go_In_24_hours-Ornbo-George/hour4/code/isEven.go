package main

import (
	"fmt"
)

func isEven(i *int) bool {
	return *i % 2 == 0
}

func main() {
	e, o := 0, 1
	fmt.Println(isEven(&e))
	fmt.Println(isEven(&o))
}
