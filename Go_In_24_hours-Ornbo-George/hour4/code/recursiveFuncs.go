package main

import (
	"fmt"
)

func feedMe(portion, eaten, limit int) int {
	eaten = portion + eaten
	if eaten >= limit {
		fmt.Printf("I'm full! I've eaten %d\n", eaten)
		return eaten
	}
	fmt.Printf("I'm still hungry! I've eaten %d\n", eaten)
	return feedMe(portion, eaten, limit)
}

func main() {
	fmt.Println(feedMe(1, 0, 10))
}
