package main

import (
	"fmt"
)

func sumNumbersA(numbers ...int) int {
	total := 0
	for _, number := range numbers {
		total += number
	}
	return total
}

func sumNumbersB(a int, numbers ...int) int {
	// Sum numbersB avoid the emphty call
	total := a
	for _, number := range numbers {
		total += number
	}
	return total
}

func main() {
	// here we have an emphty call
	// https://dave.cheney.net/practical-go/presentations/qcon-china.html#_prefer_var_args_to_t_parameters
	a, b := sumNumbersA(1, 2, 3, 4), sumNumbersA()
	fmt.Println(a, b)
	a, b = sumNumbersB(1, 2, 3, 4), sumNumbersB(5)
	fmt.Println(a, b)
}
