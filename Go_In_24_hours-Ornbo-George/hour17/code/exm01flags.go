package main

import (
	"flag"
	"fmt"
	"os"
	"strings"
)

func flagUsage() {
	usageText := `
		Usage example [OPTION]
		An example of customizing usage output
	    -s, --s         example string argument, default: String help text
	    -i, --i         example integer argument, default: Int help text
	    -b, --b         example boolean argument, default: Bool help text`
	fmt.Fprintf(os.Stderr, "%s\n", usageText)
}

func main() {
	s := flag.String("s", "Hello world", "String help text")
	i := flag.Int("i", 1, "Int help flag")
	b := flag.Bool("b", true, "Bool help text")
	flag.Usage = flagUsage
	uppercaseCmd := flag.NewFlagSet("uppercase", flag.ExitOnError)
	lowercaseCmd := flag.NewFlagSet("lowercase", flag.ExitOnError)
	if len(os.Args) == 1 {
		flag.Usage()
		return
	}
	switch os.Args[1] {
	case "uppercase":
		s := uppercaseCmd.String("s", "", "A string of text to be uppercased")
		uppercaseCmd.Parse(os.Args[2:])
		fmt.Println(strings.ToUpper(*s))
	case "lowercase":
		s := lowercaseCmd.String("s", "", "A string of text to belowercased")
		lowercaseCmd.Parse(os.Args[2:])
		fmt.Println(strings.ToLower(*s))
	default:
		flag.Usage()
	}

	flag.Parse()
	fmt.Println("value of s,i,b:", *s, *i, *b)
}
