package main

import (
	"fmt"
)

func echo(str string) {
	fmt.Println(str)
	return
}

func main() {
	s := "Hello World"
	t := "Goodbye Cruel World"
	for i := 0; i < 10; i++ {
		echo(s)
		echo(t)
	}
}
