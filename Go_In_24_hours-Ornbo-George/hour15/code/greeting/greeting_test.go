package greeting

import (
	"testing"
)

func TestHello(t *testing.T) {
	expected := "hello world !"
	if Hello() != expected {
		t.Errorf("Failed = expected : %v but obtained : %v", expected, Hello())
	}
}

func TestHelloBadWay(t *testing.T) {
	expected := "hello world !"
	if Hello() == expected {
		t.Errorf("Failed = expected : != %v but obtained : %v", expected, Hello())
	}
}

func TestFrTranslation(t *testing.T) {
	got := translate("fr-FR")
	want := "Bonjour "
	if got != want {
		t.Fatalf("Expected %q, got %q", want, got)
	}
}

func TestUSTranslation(t *testing.T) {
	got := Greeting("George", "en-US")
	want := "Hello George"
	if got != want {
		t.Fatalf("Expected %q, got %q", want, got)
	}
}

// Using a table test:
type GreetingTest struct {
	name   string
	locale string
	want   string
}

var greetingTests = []GreetingTest{
	{"George", "en-US", "Hello George"},
	{"Chloé", "fr-FR", "Bonjour Chloé"},
	{"Giuseppe", "it-IT", "Ciao Giuseppe"},
}

func TestGreeting(t *testing.T) {
	for _, test := range greetingTests {
		got := Greeting(test.name, test.locale)
		if got != test.want {
			t.Errorf("Greeting(%s,%s) = %v; want %v", test.name, test.locale, got, test.want)
		}
	}
}
