package greeting

func Hello() string {
	return "hello world !"
}

func translate(language string) string {
	switch language {
	case "en-US":
		return "Hello "
	case "fr-FR":
		return "Bonjour "
	case "it-IT":
		return "Ciao "
	default:
		return "Hello "
	}
}

func Greeting(name, locale string) string {
	return translate(locale) + name
}
